<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */
namespace OCA\Purge\Controller;

use OCP\AppFramework\Http\JSONResponse;
use OCP\BackgroundJob\IJobList;
use OCP\IGroupManager;
use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Controller;
use OCP\IL10N;
use OCP\IConfig;
use OCP\IUserManager;

class SettingsController extends Controller
{
    /** @var IL10N $l10n */
    private $l10n;

    /** @var IConfig $config */
    private $config;

    /** @var IUserManager $userManager */
    private $userManager;

	/** @var IGroupManager $groupManager */
    private $groupManager;

	/** @var IJobList $jobList */
	private $jobList;

    protected $appName;

    public function __construct($appName, IRequest $request, IL10N $l10n, IConfig $config, IUserManager $userManager, IGroupManager $groupManager, IJobList $jobList)
    {
        $this->l10n = $l10n;
        $this->config = $config;
        $this->appName = $appName;
        $this->userManager = $userManager;
        $this->groupManager = $groupManager;
		$this->jobList = $jobList;
        parent::__construct($appName, $request);
    }

    /**
     * @AdminRequired
     *
     * @return JSONResponse
     */
    public function purge() {

    	$activated = false;
		$new = false;
		if ($this->config->getAppValue($this->appName, 'activate_purge', false)) {
			$activated = true;
			if (!$this->jobList->has('OCA\Purge\Cron\PurgeDeactivated', null)) {
				$new = true;
				$this->jobList->add('OCA\Purge\Cron\PurgeDeactivated', null);
			}
		}

        return new JSONResponse(
        	[
        		'activated' => $activated,
				'new' => $new
			]
		);
    }


    /**
     * @AdminRequired
     *
     * @return TemplateResponse
     */
    public function displayPanel()
    {
        $time_before_first_warning = $this->config->getAppValue($this->appName, 'time_before_first_warning', '');
        $type_of_time_1 = $this->config->getAppValue($this->appName, 'type_of_time_1', 'days');

        $time_before_deactivation = $this->config->getAppValue($this->appName, 'time_before_deactivation', '');
        $type_of_time_2 = $this->config->getAppValue($this->appName, 'type_of_time_2', 'days');

        $nb_purged_accounts = $this->config->getAppValue($this->appName, 'nb_users_deleted', '0');
        $nb_warned_accounts = $this->config->getAppValue($this->appName, 'warned_users', '0');
        $nb_deactivated_accounts = $this->config->getAppValue($this->appName, 'nb_deactivated_accounts', '0');

        $activate_purge = $this->config->getAppValue($this->appName, 'activate_purge', false);

		$debug = $this->config->getSystemValue('debug', false);

        return new TemplateResponse('purge', 'admin', [
            'time_before_first_warning' => $time_before_first_warning,
            'type_of_time_1' => $type_of_time_1,
            'time_before_deactivation' => $time_before_deactivation,
            'type_of_time_2' => $type_of_time_2,
            'nb_users_deleted' => $nb_purged_accounts,
            'nb_warned_accounts' => $nb_warned_accounts,
            'nb_deactivated_accounts' => $nb_deactivated_accounts,
            'activate_purge' => $activate_purge,
			'debug' => $debug
        ], '');
    }
}
