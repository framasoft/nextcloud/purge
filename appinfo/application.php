<?php
/**
 * ownCloud - purge.
 *
 * This file is licensed under the Affero General Public License version 3 or
 * later. See the COPYING file.
 *
 * @author Thomas Citharel <tcit@tcit.fr>
 */
namespace OCA\Purge\AppInfo;

use OC\AppFramework\Utility\SimpleContainer;
use OCA\Purge\Cron\LoginHook;
use OCP\AppFramework\App;
use OCA\Purge\Controller\SettingsController;

class Application extends App
{
    public function __construct(array $urlParams = array())
    {
        parent::__construct('purge', $urlParams);

        $container = $this->getContainer();

        /*
         * Controllers
         */

        $container->registerService('SettingsController', function (SimpleContainer $c) {
            return new SettingsController(
                $c->query('AppName'),
                $c->query('Request'),
                $c->query('L10N'),
                $c->query('Config'),
                $c->query('UserManager'),
                $c->query('GroupManager'),
				$c->query('JobList')
            );
        });

        /*
         * Core
         */
        $container->registerService('UserManager', function (SimpleContainer $c) {
            return $c->query('ServerContainer')->getUserManager();
        });

        $container->registerService('GroupManager', function (SimpleContainer $c) {
            return $c->query('ServerContainer')->getGroupManager();
        });

		$container->registerService('JobList', function (SimpleContainer $c) {
			return $c->query('ServerContainer')->getJobList();
		});

        $container->registerService('Config', function (SimpleContainer $c) {
            return $c->query('ServerContainer')->getConfig();
        });

        $container->registerService('Mailer', function (SimpleContainer $c) {
            return $c->query('ServerContainer')->getMailer();
        });

        $container->registerService('L10N', function (SimpleContainer $c) {
            return $c->query('ServerContainer')->getL10N($c->query('AppName'));
        });

        $container->registerService('Defaults', function () {
            return new \OC_Defaults();
        });

        $container->registerService('LoginHook', function (SimpleContainer $c) {
            return new LoginHook(
                $c->query('ServerContainer')->getUserSession(),
                $c->query('ServerContainer')->getConfig()
            );
        });
    }
}
