<table cellspacing="0" cellpadding="0" border="0" width="100%">
	<tr><td>
			<table cellspacing="0" cellpadding="0" border="0" width="600px">
				<tr>
					<td bgcolor="<?php p($theme->getMailHeaderColor());?>" width="20px">&nbsp;</td>
					<td bgcolor="<?php p($theme->getMailHeaderColor());?>">
						<img src="<?php p(OC_Helper::makeURLAbsolute(image_path('', 'logo-mail.gif'))); ?>" alt="<?php p($theme->getName()); ?>"/>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td width="20px">&nbsp;</td>
					<td style="font-weight:normal; font-size:0.8em; line-height:1.2em; font-family:verdana,'arial',sans;">
						<?php
                        print_unescaped($l->t("Bonjour,<br><br>Votre compte sur %s n'a pas été utilisé depuis <strong>%s %s</strong>. ", [$_['sitename'], $_['time1'], $_['type1']]));
                        print_unescaped($l->t('Nous essayons de faire de la place pour tout le monde en supprimant les comptes inutilisés.<br>'));

						print_unescaped($l->t('<br><br>'));

                        print_unescaped($l->t("<strong>Votre compte sera automatiquement supprimé après %s %s si vous ne vous connectez pas entre temps.</strong> Si vous n'utilisez plus %s, merci de nous le notifier sur <a href='https://contact.framasoft.org/#framadrive'>https://contact.framasoft.org/#framadrive</a>.", [$_['time2'], $_['type2'], $_['sitename']]));
						print_unescaped($l->t("<br>Rappel : votre nom d'utilisateur est %s.", $_['username']));

                        print_unescaped($l->t('<br><br>'));
                        // TRANSLATORS term at the end of a mail
                        print_unescaped($l->t('Bonne journée !'));
                        ?>
					</td>
				</tr>
				<tr><td colspan="2">&nbsp;</td></tr>
				<tr>
					<td width="20px">&nbsp;</td>
					<td style="font-weight:normal; font-size:0.8em; line-height:1.2em; font-family:verdana,'arial',sans;">--<br>
						<?php p($theme->getName()); ?> -
						<?php p($theme->getSlogan()); ?>
						<br><a href="<?php p($theme->getBaseUrl()); ?>"><?php p($theme->getBaseUrl());?></a>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td></tr>
</table>
